
.PHONY: orrs plotlib bins clean

all: orrs/osmap.dat plotlib bins

orrs:
	$(MAKE) -C orrs/bin/ -f Makefile_DP osgen osmap.o

orrs/osmap.dat:	orrs
	cd orrs && bin/osgen ./osmaps_ns.lst

libPlt_gDP.a:
	$(MAKE) -C plotlib libPlt_gDP.a

bins: libPlt_gDP.a
	$(MAKE) -C bin xfoil pplot pxplot

clean:
	rm -f $(wildcard ~/bin/xfoil ~/bin/pplot ~/bin/pxplot)
	$(MAKE) -C orrs/bin -f Makefile_DP clean
	$(MAKE) -C plotlib clean
	$(MAKE) -C bin clean
